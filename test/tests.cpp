#include <stdlib.h>
#include <stdio.h>


#include <decomphelmholtz.h>



// ================ grid size setting ==================
#ifndef NX
#define NX 128
#endif
#ifndef NY
#define NY 128
#endif
#ifndef NZ
#define NZ 128
#endif



template <int nx, int ny, int nz>
void lamb_ossen_vortex(double* vx, double* vy, double* vz, const double sigma, const double dx=1.0)
{
    const int    nxyz   = nx*ny*nz;
    const double sigma2 = sigma*sigma;
    
    for (int ix=0; ix<nx; ix++)
    for (int iy=0; iy<ny; iy++)
    for (int iz=0; iz<nz; iz++)
    {
        const int ixyz = ix*ny*nz + iy*nz + iz; // index in array
        
        double x = ((double) (ix-nx/2))*dx;
        double y = ((double) (ix-ny/2))*dx;
        double z = ((double) (ix-nz/2))*dx;
        
        double r   = sqrt(x*x + y*y);
        double phi = atan2(y,x);
        
        if ( r < 1e-10) r=1e-10;
        
        double v = (1 - exp(-r*r/(2.0*sigma2))) / r;
        
        vx[ixyz] = -sin(phi) * v;
        vy[ixyz] =  cos(phi) * v;
        vz[ixyz] = 0.0;
    }
}


template <int nx, int ny, int nz>
void compare_arrays(double* data1, double* data2,
                    double* mean_diff, double* max_diff, double* stddev_diff)
{
    const int    nxyz   = nx*ny*nz;
    *max_diff = 0.0;
    
    for (int ix=0; ix<nx; ix++)
    for (int iy=0; iy<ny; iy++)
    for (int iz=0; iz<nz; iz++)
    {
        const int ixyz = ix*ny*nz + iy*nz + iz; // index in array
        
        double diff = fabs(data1[ixyz] - data2[ixyz]);
        
        *mean_diff   +=  diff    / ((double) nxyz);
        *stddev_diff +=  pow(diff,2.0) / ((double) nxyz);
        
        if (*max_diff < diff) *max_diff=diff;
    }
    
    *stddev_diff = sqrt(  (*stddev_diff) - (*mean_diff)*(*mean_diff)  );
    
}



/*
 * g++ tests.cpp -o tests.x -fopenmp -I. -I.. -lfftw3 -lgomp
 */
int main(int argc, char** argv)
{
    const int nxyz = NX*NY*NZ;
    
    double* vx = (double*) malloc( nxyz * sizeof(double) );
    double* vy = (double*) malloc( nxyz * sizeof(double) );
    double* vz = (double*) malloc( nxyz * sizeof(double) );
    
    double* vcx = (double*) malloc( nxyz * sizeof(double) );
    double* vcy = (double*) malloc( nxyz * sizeof(double) );
    double* vcz = (double*) malloc( nxyz * sizeof(double) );
    
    double* vix = (double*) malloc( nxyz * sizeof(double) );
    double* viy = (double*) malloc( nxyz * sizeof(double) );
    double* viz = (double*) malloc( nxyz * sizeof(double) );
    
    double v0x,v0y,v0z;
    
    // here initialize vx,vy,vz
    double sigma = ((double) NX)*0.1;
    lamb_ossen_vortex<NX,NY,NZ>(vx,vy,vz,sigma);
    
    
    // perform helmholtz decomposition
    helmholz_decomp(vx,  vy,  vz,
                    vcx, vcy, vcz,
                    vix, viy, viz,
                    NULL, NULL, NULL,
                    &v0x, &v0y, &v0z,
                    NX, NY, NZ);
    
    
    // comapre
    double mean_diff   = 0.0;
    double stddev_diff = 0.0;
    double max_diff    = 0.0;
    
    compare_arrays<NX,NY,NZ>(vx, vix, &mean_diff, &max_diff, &stddev_diff);
    
    printf("mean diff. vx:  %lf \n",mean_diff);
    printf("std. diff. vx:  %lf \n",stddev_diff);
    printf("max  diff. vx:  %lf \n",max_diff);
    
    
    printf("v0 = [%10.5lf,%10.5lf,%10.5lf]\n",v0x,v0y,v0z);
    
    
    finalize_helmholz_decomp();
    if (vx  != NULL) free(vx);
    if (vy  != NULL) free(vy);
    if (vz  != NULL) free(vz);
    if (vcx != NULL) free(vcx);
    if (vcy != NULL) free(vcy);
    if (vcz != NULL) free(vcz);
    if (vix != NULL) free(vix);
    if (viy != NULL) free(viy);
    if (viz != NULL) free(viz);
    
    return EXIT_SUCCESS;
}
