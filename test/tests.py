from __future__ import print_function, division

import unittest
import inspect

import os
import sys

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mplcolors
#import mpl_toolkits.axes_grid1.make_axes_locatable


# import custom modules
pwd = os.path.dirname(os.path.realpath(__file__))
print('# working in directory: ',pwd)
print('# adding to path: ',pwd+'/..')
sys.path.append(pwd+'/..')
import _decomphelmholtz as decomp

# see: https://stackoverflow.com/questions/18195758/set-matplotlib-colorbar-size-to-match-graph
#def get_cbar_axis(ax,position='bottom'):
#    return make_axes_locatable(ax).append_axes(position, size="5%", pad=0.05)

def plot_vector_field_2d(fig,ax,X,Y,vx,vy,stride=1,vmin=None,vmax=None,
                         title=r'',label=r'',cmap='viridis',normalize=mplcolors.Normalize):
    """
    https://stackoverflow.com/questions/18195758/set-matplotlib-colorbar-size-to-match-graph
    """
    # compute magnitude of vector field
    mgn = np.sqrt(vx**2 + vy**2)

    if vmax is None:
        vmax = mgn.max()
    if vmin is None:
        vmin = mgn.min()
    if np.abs(vmin) < 1e-10:
        vmin += 1e-10
    

    ax.set_title(title)
    ax.set_aspect('equal')
    im = ax.pcolor(X,Y, mgn, norm=normalize(vmin=vmin,vmax=vmax), cmap=cmap )
    qv = ax.quiver(X[::stride,::stride],Y[::stride,::stride],vx[::stride,::stride],vy[::stride,::stride] )
    cbar = fig.colorbar(im,ax=ax,orientation='horizontal',fraction=0.046, pad=0.04)
    cbar.set_label(label)


def logPoint(context):
    'utility function used for module functions and class methods'
    callingFunction = inspect.stack()[1][3]
    print( 'in %s - %s()' % (context, callingFunction) )
 
 
class TestHelmholtz(unittest.TestCase):
    # set grid settings for tests
    nx = 256
    ny = 256
    nz = 128
    dx = 1.0

 
    @classmethod
    def setUpClass(self):
        # called once, before any tests
        logPoint('class %s' % self.__name__)

        # initialize grid for tests
        self.x = np.linspace(-0.5*self.nx*self.dx, 0.5*self.nx*self.dx, self.nx, endpoint=False)
        self.y = np.linspace(-0.5*self.ny*self.dx, 0.5*self.ny*self.dx, self.ny, endpoint=False)
        self.z = np.linspace(-0.5*self.nz*self.dx, 0.5*self.nz*self.dx, self.nz, endpoint=False)

        self.X = None
        self.Y = None
        self.Z = None

        self.X, self.Y, self.Z = np.meshgrid(self.x,self.y,self.z,indexing='ij')
        self.R   = np.sqrt( self.X**2 + self.Y**2 )
        self.Phi = np.arctan2(self.Y,self.X)

        # we need finite value instead of 0.0
        self.R[np.where(self.R<1e-10)] = 1e-10
 
    @classmethod
    def tearDownClass(cls):
        'called once, after all tests, if setUpClass successful'
        logPoint('class %s' % cls.__name__)
 
    def setUp(self):
        'called multiple times, before every test method'
        self.logPoint()
 
    def tearDown(self):
        'called multiple times, after every test method'
        self.logPoint()
 
    def test_lamboseenvortex(self):
        self.logPoint()
        sigma2 = (0.5*0.05*self.nx)**2

        v  = ( 1.0-np.exp(-self.R**2/sigma2) ) / self.R
        v[0,0,:] = 0.0
        vx = np.ascontiguousarray( -np.sin(self.Phi) * v )
        vy = np.ascontiguousarray(  np.cos(self.Phi) * v )
        vz = np.ascontiguousarray(  np.zeros(v.shape,dtype=np.float64,order='c') )

        # memory buffers for 
        vcx = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        vcy = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        vcz = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        
        # memory buffers
        vix = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        viy = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        viz = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')

        decomp.helmholtz(vx,  vy,  vz,
                         vcx, vcy, vcz,
                         vix, viy, viz)
        
        # as the vortex field should be only incompressible 

        diff_x = np.abs(vx-vix)
        diff_y = np.abs(vy-viy)
        #diff_z = vz-viz

        print( '# mean difference vx/vix', diff_x.mean() )
        print( '# mean difference vy/viy', diff_y.mean() )
        print( '# max difference vx/vix ', diff_x.max() )
        print( '# max difference vy/viy ', diff_y.max() )


        fig, axes = plt.subplots(nrows=2,ncols=3,figsize=[30.,20.])
        ax1,ax2,ax3,ax4,ax5,ax6 = axes.ravel()

        #fig.suptitle('Lamb-Oseen vortex')
        #plt.title('Lamb-Oseen vortex')

        plot_vector_field_2d(fig,ax1,self.X[:,:,0],self.Y[:,:,0],vx[:,:,0],vy[:,:,0], stride=4,
                             title=r'full velocity field $\mathbf{v}$',
                             label=r'magnitude $\vert \mathbf{v} \vert$')
        
        plot_vector_field_2d(fig,ax2,self.X[:,:,0],self.Y[:,:,0],vix[:,:,0],viy[:,:,0], stride=4,
                             title=r'incompressible $\mathbf{v}_i$',
                             label=r'magnitude $\vert \mathbf{v}_i \vert$')

        plot_vector_field_2d(fig,ax3,self.X[:,:,0],self.Y[:,:,0],vcx[:,:,0],vcy[:,:,0], stride=4,
                             normalize=mplcolors.LogNorm,
                             title=r'compressible $\mathbf{v}_c$',
                             label=r'magnitude $\vert \mathbf{v}_c \vert$')

        # show spatial error magnitude
        plot_vector_field_2d(fig,ax4,self.X[:,:,0],self.Y[:,:,0],vx[:,:,0]-vix[:,:,0],
                                                                 vy[:,:,0]-viy[:,:,0],
                             normalize=mplcolors.LogNorm,stride=4,cmap='Reds',
                             title=r'error magnitude',
                             label=r'magnitude $\vert \mathbf{v}-\mathbf{v}_i \vert$')


        # compare radial plot of velocity magnitudes
        ax5.set_title('Comparison of full and incomp. velocity fields')
        ax5.set_xlim([0.,self.R.max()])
        ax5.grid(True)
        mgn_err = np.sqrt(diff_x**2 + diff_y**2)
        im = ax5.scatter( self.R[:,:,0].flatten(), np.sqrt(vix[:,:,0]**2+viy[:,:,0]**2).flatten(), 
                          marker='o',color='none',edgecolor='k',label='after decomposition')
        im = ax5.scatter( self.R[:,:,0].flatten(), np.sqrt(vx[:,:,0]**2+vy[:,:,0]**2).flatten(), 
                          alpha=0.5, c=mgn_err[:,:,0].flatten(), cmap='Reds',label='before decomposition')
        cbar = fig.colorbar(im,ax=ax5,orientation='horizontal',fraction=0.046, pad=0.04)
        ax5.legend(loc='best')
        

        ax6.set_xlim([0.,self.R.max()])
        ax6.scatter( self.R[:,:,0].flatten(), np.sqrt(vcx[:,:,0]**2+vcy[:,:,0]**2).flatten(), color='k')

        fig.tight_layout()
        plt.savefig('lamb-ossen-test.png')





 
    def test_gaussianvortex(self):
        self.logPoint()
        sigma2 = (0.5*0.2*self.nx)**2

        v  = np.exp(-0.5*self.R**2/sigma2)
        vx = np.ascontiguousarray( -np.sin(self.Phi) * v )
        vy = np.ascontiguousarray(  np.cos(self.Phi) * v )
        vz = np.ascontiguousarray(  np.zeros(v.shape,dtype=np.float64,order='c') )

        # memory buffers for 
        vcx = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        vcy = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        vcz = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        
        # memory buffers
        vix = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        viy = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        viz = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')

        decomp.helmholtz(vx,  vy,  vz,
                         vcx, vcy, vcz,
                         vix, viy, viz)
        
        # as the vortex field should be only incompressible 

        diff_x = np.abs(vx-vix)
        diff_y = np.abs(vy-viy)
        #diff_z = vz-viz

        print( '# mean difference vx/vix', diff_x.mean() )
        print( '# mean difference vy/viy', diff_y.mean() )
        print( '# max difference vx/vix ', diff_x.max() )
        print( '# max difference vy/viy ', diff_y.max() )


        fig, axes = plt.subplots(nrows=2,ncols=3,figsize=[30.,20.])
        ax1,ax2,ax3,ax4,ax5,ax6 = axes.ravel()

        #fig.suptitle('Lamb-Oseen vortex')
        #plt.title('Lamb-Oseen vortex')

        plot_vector_field_2d(fig,ax1,self.X[:,:,0],self.Y[:,:,0],vx[:,:,0],vy[:,:,0], stride=4,
                             title=r'full velocity field $\mathbf{v}$',
                             label=r'magnitude $\vert \mathbf{v} \vert$')
        
        plot_vector_field_2d(fig,ax2,self.X[:,:,0],self.Y[:,:,0],vix[:,:,0],viy[:,:,0], stride=4,
                             title=r'incompressible $\mathbf{v}_i$',
                             label=r'magnitude $\vert \mathbf{v}_i \vert$')

        plot_vector_field_2d(fig,ax3,self.X[:,:,0],self.Y[:,:,0],vcx[:,:,0],vcy[:,:,0], stride=4,
                             normalize=mplcolors.LogNorm,
                             title=r'compressible $\mathbf{v}_c$',
                             label=r'magnitude $\vert \mathbf{v}_c \vert$')

        # show spatial error magnitude
        plot_vector_field_2d(fig,ax4,self.X[:,:,0],self.Y[:,:,0],vx[:,:,0]-vix[:,:,0],
                                                                 vy[:,:,0]-viy[:,:,0],
                             normalize=mplcolors.LogNorm,stride=4,cmap='Reds',
                             title=r'error magnitude',
                             label=r'magnitude $\vert \mathbf{v}-\mathbf{v}_i \vert$')


        # compare radial plot of velocity magnitudes
        ax5.set_title('Comparison of full and incomp. velocity fields')
        ax5.set_xlim([0.,self.R.max()])
        ax5.grid(True)
        mgn_err = np.sqrt(diff_x**2 + diff_y**2)
        im = ax5.scatter( self.R[:,:,0].flatten(), np.sqrt(vix[:,:,0]**2+viy[:,:,0]**2).flatten(), 
                          marker='o',color='none',edgecolor='k',label='after decomposition')
        im = ax5.scatter( self.R[:,:,0].flatten(), np.sqrt(vx[:,:,0]**2+vy[:,:,0]**2).flatten(), 
                          alpha=0.5, c=mgn_err[:,:,0].flatten(), cmap='Reds',label='before decomposition')
        cbar = fig.colorbar(im,ax=ax5,orientation='horizontal',fraction=0.046, pad=0.04)
        ax5.legend(loc='best')
        

        ax6.set_xlim([0.,self.R.max()])
        ax6.scatter( self.R[:,:,0].flatten(), np.sqrt(vcx[:,:,0]**2+vcy[:,:,0]**2).flatten(), color='k')

        fig.tight_layout()
        plt.savefig('gaussian-test.png')

    def test_diffgaussianvortex(self):
        self.logPoint()
        sigma2 = (0.5*0.2*self.nx)**2

        v  = self.R*np.exp(-0.5*self.R**2/sigma2)
        vx = np.ascontiguousarray( -np.sin(self.Phi) * v )
        vy = np.ascontiguousarray(  np.cos(self.Phi) * v )
        vz = np.ascontiguousarray(  np.zeros(v.shape,dtype=np.float64,order='c') )

        # memory buffers for 
        vcx = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        vcy = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        vcz = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        
        # memory buffers
        vix = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        viy = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')
        viz = np.empty((self.nx,self.ny,self.nz),dtype=np.float64,order='c')

        decomp.helmholtz(vx,  vy,  vz,
                         vcx, vcy, vcz,
                         vix, viy, viz)
        
        # as the vortex field should be only incompressible 

        diff_x = np.abs(vx-vix)
        diff_y = np.abs(vy-viy)
        #diff_z = vz-viz

        print( '# mean difference vx/vix', diff_x.mean() )
        print( '# mean difference vy/viy', diff_y.mean() )
        print( '# max difference vx/vix ', diff_x.max() )
        print( '# max difference vy/viy ', diff_y.max() )


        fig, axes = plt.subplots(nrows=2,ncols=3,figsize=[30.,20.])
        ax1,ax2,ax3,ax4,ax5,ax6 = axes.ravel()

        #fig.suptitle('Lamb-Oseen vortex')
        #plt.title('Lamb-Oseen vortex')

        plot_vector_field_2d(fig,ax1,self.X[:,:,0],self.Y[:,:,0],vx[:,:,0],vy[:,:,0], stride=4,
                             title=r'full velocity field $\mathbf{v}$',
                             label=r'magnitude $\vert \mathbf{v} \vert$')
        
        plot_vector_field_2d(fig,ax2,self.X[:,:,0],self.Y[:,:,0],vix[:,:,0],viy[:,:,0], stride=4,
                             title=r'incompressible $\mathbf{v}_i$',
                             label=r'magnitude $\vert \mathbf{v}_i \vert$')

        plot_vector_field_2d(fig,ax3,self.X[:,:,0],self.Y[:,:,0],vcx[:,:,0],vcy[:,:,0], stride=4,
                             normalize=mplcolors.LogNorm,
                             title=r'compressible $\mathbf{v}_c$',
                             label=r'magnitude $\vert \mathbf{v}_c \vert$')

        # show spatial error magnitude
        plot_vector_field_2d(fig,ax4,self.X[:,:,0],self.Y[:,:,0],vx[:,:,0]-vix[:,:,0],
                                                                 vy[:,:,0]-viy[:,:,0],
                             normalize=mplcolors.LogNorm,stride=4,cmap='Reds',
                             title=r'error magnitude',
                             label=r'magnitude $\vert \mathbf{v}-\mathbf{v}_i \vert$')


        # compare radial plot of velocity magnitudes
        ax5.set_title('Comparison of full and incomp. velocity fields')
        ax5.set_xlim([0.,self.R.max()])
        ax5.grid(True)
        mgn_err = np.sqrt(diff_x**2 + diff_y**2)
        im = ax5.scatter( self.R[:,:,0].flatten(), np.sqrt(vix[:,:,0]**2+viy[:,:,0]**2).flatten(), 
                          marker='o',color='none',edgecolor='k',label='after decomposition')
        im = ax5.scatter( self.R[:,:,0].flatten(), np.sqrt(vx[:,:,0]**2+vy[:,:,0]**2).flatten(), 
                          alpha=0.5, c=mgn_err[:,:,0].flatten(), cmap='Reds',label='before decomposition')
        cbar = fig.colorbar(im,ax=ax5,orientation='horizontal',fraction=0.046, pad=0.04)
        ax5.legend(loc='best')
        

        ax6.set_xlim([0.,self.R.max()])
        ax6.scatter( self.R[:,:,0].flatten(), np.sqrt(vcx[:,:,0]**2+vcy[:,:,0]**2).flatten(), color='k')

        fig.tight_layout()
        plt.savefig('diffgaussian-test.png')

 
 
    def logPoint(self):
        'utility method to trace control flow'
        callingFunction = inspect.stack()[1][3]
        currentTest = self.id().split('.')[-1]
        print('in %s - %s()' % (currentTest, callingFunction))


if __name__ == '__main__':

    TestHelmholtz.nz = 256

    unittest.main()
