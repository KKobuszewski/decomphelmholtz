

# Tests of the code for Helmholtz decomposition

## Running tests


To run tests type:

```
python tests.py
```

Prerequests

* numpy
* ...

This directory have to be inside decomphelmholtz library directory.



# Test cases

## Typical velocity distribution

This code is made to acurately compute Helmholtz decomposition for velocity fields
that are vanishing at boundaries. Look at example below.

![alt text](diffgaussian-test.png "")


## First issue

Finite value of the field at axis of rotation gives wrong values... 
TODO: This should be corrected.

![alt text](gaussian-test.png "")


## Second issue - finite value at boundaries

The decomposition for field with finite value at boundary is done wrongly due to
Fourier transfrom utilization.


![alt text](lamb-ossen-test.png "")

