# decomphelmholtz
Code for Helmholtz decomposition of a vector field (periodic boundary problems).


## Algorithm description

For a given vector field $` \mathbf{w}(\mathbf{r}) `$ we consdier its Fourier transform

```math
\widehat{\mathbf{w}}(\mathbf{k}) = \mathcal{F} \left[ \mathbf{w}(\mathbf{r}) \right]
```


```math
\widehat{\mathbf{w}}(\mathbf{k}) = 
-i \mathbf{k} \cdot  \left( i\frac{\mathbf{k}}{k^2} \cdot  \widehat{\mathbf{w}}(\mathbf{k}) \right) + 
 i \mathbf{k} \times \left( i\frac{\mathbf{k}}{k^2} \times \widehat{\mathbf{w}}(\mathbf{k}) \right) + \widehat{\mathbf{w}}(0)
```

We can call two parts of the field as compressible
```math
\widehat{\mathbf{w}}_{\parallel}(\mathbf{k}) = \frac{\mathbf{k}}{k^2} \cdot  \left( \mathbf{k} \cdot  \widehat{\mathbf{w}}(\mathbf{k}) \right) 
```
and incompressible
```math
\widehat{\mathbf{w}}_{\bot}(\mathbf{k}) = 
\frac{\mathbf{k}}{k^2} \times  \left( \mathbf{k} \times  \widehat{\mathbf{w}}(\mathbf{k}) \right) = 
\widehat{\mathbf{w}}(\mathbf{k}) - \widehat{\mathbf{w}}_{\parallel}(\mathbf{k})
```
since $` \widehat{\mathbf{w}}_{\parallel}(0) = \widehat{\mathbf{w}}_{\bot}(0) =0 `$.

~\\


Quantities
```math
\varphi(\mathbf{r})    = \mathcal{F}^{-1} \left[  i\frac{\mathbf{k}}{k^2} \cdot  \widehat{\mathbf{w}}(\mathbf{k}) \right]
```
and
```math
\mathbf{a}(\mathbf{r}) = \mathcal{F}^{-1} \left[  i\frac{\mathbf{k}}{k^2} \times  \widehat{\mathbf{w}}(\mathbf{k}) \right]
```
are scalar and vector potential of the field $`\mathbf{w}`$, respectively.



In the code we assume that vector field is implemented on a regular cubic grid so the Fourier transform can be performed with FFTW functions.





## Authors

K. Sekizawa
K. Kobuszewski
